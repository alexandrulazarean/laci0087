package BibliotecaApp.control;

import BibliotecaApp.model.Carte;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class BibliotecaCtrlTest {
    Carte C1,C2,C3,C4,C5,C6,C7,C8;
    static BibliotecaCtrl ctrl;

    @BeforeClass
    public static void setup(){
        ctrl=new BibliotecaCtrl();

    }

    @Before
    public void setUp() throws Exception {
        List<String> autoriC1 = new ArrayList<>(Arrays.asList("Mircea Eliade"));
        List<String> cuvinteCheieC1 = new ArrayList<>(Arrays.asList("Sacrul","Mircea"));
        List<String> autoriC2 = new ArrayList<>(Arrays.asList("Mircea Eliade"));
        List<String> cuvinteCheieC2 = new ArrayList<>(Arrays.asList("Viata","Darius"));
        List<String> autoriC3 = new ArrayList<>(Arrays.asList("Oana Ionescu","Vlad Radutiu"));
        List<String> cuvinteCheieC3 = new ArrayList<>(Arrays.asList("Antichitate","Vlad"));
        List<String> autoriC4 = new ArrayList<>(Arrays.asList( "Osho"));
        List<String> cuvinteCheieC4 = new ArrayList<>(Arrays.asList("Osho","Calatoria"));
        List<String> autoriC5 = new ArrayList<>(Arrays.asList( "George Liviu"));
        List<String> cuvinteCheieC5 = new ArrayList<>(Arrays.asList("George","Osho","Publica"));
        List<String> autoriC6 = new ArrayList<>(Arrays.asList( "Mahatma Gandhi"));
        List<String> cuvinteCheieC6 = new ArrayList<>(Arrays.asList("Viata","Guru","Gandhi"));
        List<String> autoriC7 = new ArrayList<>(Arrays.asList( "Maria Ion"));
        List<String> cuvinteCheieC7 = new ArrayList<>(Arrays.asList("Maria","Ochii"));
        List<String> autoriC8 = new ArrayList<>(Arrays.asList( "Maria Ion"));
        List<String> cuvinteCheieC8 = new ArrayList<>(Arrays.asList("Maria","Ochii"));

        C1= new Carte("Sacrul si profanul",autoriC1, "1959", "Humanitasaaa",cuvinteCheieC1);
        C2= new Carte("Viata lui Darius Enache Dssmnjdfhdbvbvndxbcvxmkcdscjddalsxlkssdlsdflksdfjksfsdckddfdnvdfc,lkjnffvfdgdfgdfdgdfdfbfbh",autoriC2, "1980", "Literatura1333",cuvinteCheieC2);
        C3= new Carte("Din antichitate in zilelele noastre",autoriC3, "1935", "Uniune",cuvinteCheieC3);
        C4= new Carte("Calatoria devenirii umane",autoriC4, "1755", "Romantica123",cuvinteCheieC4);
        C5= new Carte("Oshooshoo",autoriC5, "1955", "Publica",cuvinteCheieC5);
        C6= new Carte("Viata unui guru modern",autoriC6, "2010", "Zambetul123456789098765yfhf09987gghjopiophiooooooo",cuvinteCheieC6);
        C7= new Carte("Undeva departe de ochii lumii",autoriC7, "1800", "Bucurestii",cuvinteCheieC7);
        C8= new Carte("Undeva departe de ochii lumii",autoriC8, "2020", "Bucurestii",cuvinteCheieC8);
        System.out.println("in BeforeTest");
    }

    @After
    public void tearDown() throws Exception {
        C1 = null;
        C2 = null;
        C3 = null;
        C4 = null;
        C5 = null;
        C6 = null;
        C7 = null;
        C8 = null;
        System.out.println("in AfterTest");
    }

    @Test(expected= Exception.class)
    public void testCarteECvalid() throws Exception {
        ctrl=new BibliotecaCtrl();

        ctrl.adaugaCarte(C1 );


    }

    @Test(expected=Exception.class)
    public void testCarteTitluECnonvalid() throws Exception {
        ctrl=new BibliotecaCtrl();

        ctrl.adaugaCarte(C2 );

    }

    @Test(expected=Exception.class)
    public void testCarteEdituraECnonvalid() throws Exception {
        ctrl=new BibliotecaCtrl();
        ctrl.adaugaCarte(C3 );
    }

    @Test(expected=Exception.class)
    public void testCarteAnAparitieECnonvalid() throws Exception {
        ctrl=new BibliotecaCtrl();

        ctrl.adaugaCarte(C4 );
    }

    @Test(expected=Exception.class)
    public void testCarteTitluBVAnonvalid() throws Exception {
        ctrl=new BibliotecaCtrl();
        ctrl.adaugaCarte(C5 );
    }

    @Test(expected=Exception.class)
    public void testCarteBVAvalid() throws Exception {
        ctrl=new BibliotecaCtrl();
        ctrl.adaugaCarte(C6 );
    }

    @Test(expected=Exception.class)
    public void testCartetTitluBVAvalid() throws Exception {
        ctrl.adaugaCarte(C7 );
    }

    @Test(expected=Exception.class)
    public void testCartetAnAparoitieBVAnonvalid() throws Exception {
        ctrl.adaugaCarte(C8 );
    }
}