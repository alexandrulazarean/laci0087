package BibliotecaApp.model;

import org.junit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CarteTest {

    Carte c,c1;
    @Before
    public void setUp() throws Exception {
        c = new Carte();
        c.setTitlu("Intampinarea crailor");
        System.out.println("in setup before test");
        c.setEditura("Litera");
        c.setAnAparitie("1995");
        c.setEditura("CarteaNoua");
        ArrayList<String> a = new ArrayList<>();
    }

    //region Laborator 1
    @After
    public void tearDown() throws Exception {
        c = null;
        System.out.println("after test");
    }

    @Test
    public void getTitlu() {
        assertEquals("Povesti",c.getTitlu());
    }

    @Ignore("setTitlu")
    @Test
    public void setTitlu() {
        c.setTitlu("Prezent");
        assertEquals("Prezent",c.getTitlu());
    }

    @BeforeClass
    public static void Setup(){
        System.out.println("before any test");
    }

    @AfterClass
    public static void Teardown(){
        System.out.println("after all test");
    }

    @Test(timeout = 100)
    public void getEditura() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(c.getEditura(), "Litera");

    }

//    @Ignore("setTitlu")
//    @Test(expected = Carte.class)
//    public void testConstructor(){
//       assert c1==null;
//       throw new NullPointerException();
//    }

//endregion

    //region Tema
    @Test
    public void getAnAparitie() {
        assertEquals("1992", c.getAnAparitie());
    }

    @Test(expected = Exception.class)
    public void setAnAparitie() throws Exception{
        c.setAnAparitie("1800");
    }

    //@Ignore("setEditura")
    @Test
    public void setEditura() {
        c.setEditura("Editura");
        assertEquals("Editura",c.getTitlu());
    }

    @Test
    public void setCuvinteCheie() {
        List<String> lista = new ArrayList<>();
        lista.addAll(Arrays.asList("aa", "cc", "xx"));
        c.setCuvinteCheie(lista);

        List<String> list = new ArrayList<>(lista);
        assertEquals(list, c.getCuvinteCheie());
    }

//endregion


}