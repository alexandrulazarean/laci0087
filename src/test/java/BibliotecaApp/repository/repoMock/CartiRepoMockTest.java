package BibliotecaApp.repository.repoMock;

import BibliotecaApp.model.Carte;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoMockTest {
    Carte c2=null,c3,c4;
    static CartiRepoMock repo ;
    static List<Carte> carti;
    static List<Carte> cartiGasite;

    @BeforeClass
    public static void setup(){
        repo = new CartiRepoMock();
//        carti = new ArrayList<Carte>();
//        cartiGasite= new ArrayList<Carte>();
    }

    @Before
    public void setUp() throws Exception {
        List<String> autoriC1 = new ArrayList<>(Arrays.asList("Mircea Eliade"));
        List<String> autoriC2 = new ArrayList<>(Arrays.asList( "Osho"));
        List<String> autoriC3 = new ArrayList<>(Arrays.asList( "Osho Liviu"));
        List<String> autoriC4 = new ArrayList<>(Arrays.asList( "Maria Ion"));

        List<String> cuvinteCheieC1 = new ArrayList<>(Arrays.asList("Sacrul","Mircea"));
        List<String> cuvinteCheieC2 = new ArrayList<>(Arrays.asList("Osho","Calatoria"));
        List<String> cuvinteCheieC3 = new ArrayList<>(Arrays.asList("George","Osho","Publica"));
        List<String> cuvinteCheieC4 = new ArrayList<>(Arrays.asList("Maria","Ochii"));

        //c1= new Carte("Sacrul si profanul",autoriC1, "1959", "Humanitasaaa",cuvinteCheieC1);
        c2= new Carte("Calatoria devenirii umane",autoriC2, "1755", "Romantica123",cuvinteCheieC2);
        c3= new Carte("Oshooshoo",autoriC3, "1955", "Publica",cuvinteCheieC3);
        c4= new Carte("Undeva departe de ochii lumii",autoriC4, "1800", "Bucurestii",cuvinteCheieC4);
    }

    @After
    public void tearDown() throws Exception {
        //c1 = null;
        c2 = null;
        c3 = null;
        c4= null;
        System.out.println("in AfterTest");
    }

    @Test
    public void testCautaCarteListaNull() {
        List<Carte> l = repo.cautaCarte("Vlad Maria");
        assert l.size() ==0;

    }

    @Test
    public void testCautaCarteListaNotNull() {

        repo.adaugaCarte(c2);
        List<Carte> l = repo.cautaCarte("Mircea");
        assert l.size()==0;

    }

    @Test
    public void testCautaCarteListaMore() {

        repo.adaugaCarte(c2);
        repo.adaugaCarte(c3);
        repo.adaugaCarte(c4);
        List<Carte> l =repo.cautaCarte("Osho");
        assert l.size()==2;
    }
}